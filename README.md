# Advent of Code Solutions

[![Build status](https://gitlab.com/ianhattendorf/advent-of-code/badges/master/build.svg)](https://gitlab.com/ianhattendorf/advent-of-code/commits/master)
[![Coverage report](https://gitlab.com/ianhattendorf/advent-of-code/badges/master/coverage.svg)](https://ianhattendorf.gitlab.io/advent-of-code)

## Description

Solutions for the [Advent of Code](https://adventofcode.com/) in C++.
