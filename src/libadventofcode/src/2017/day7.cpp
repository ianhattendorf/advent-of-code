#include "libadventofcode/2017/day7.hpp"

#include <regex>
#include <unordered_set>
#include <unordered_map>
#include <iostream>
#include <iterator>
#include <numeric>
#include <algorithm>

#include "libadventofcode/2017/util.hpp"

namespace advent_of_code::y2017::day7 {
	const std::regex line_regex(R"delim(^(\w+)\s+\((\d+)\)(?:\s+->\s+([\w\s,]+))?$)delim");

	/**
	 * One program at the bottom supports the entire tower. It's holding a large disc, and on the disc are balanced
	 * several more sub-towers. At the bottom of these sub-towers, standing on the bottom disc, are other programs,
	 * each holding their own disc, and so on. At the very tops of these sub-sub-sub-...-towers, many programs
	 * stand simply keeping the disc below them balanced but with no disc of their own.

	You offer to help, but first you need to understand the structure of these towers. You ask each program to yell
	 out their name, their weight, and (if they're holding a disc) the names of the programs immediately above them
	 balancing on that disc. You write this information down (your puzzle input). Unfortunately, in their panic, they
	 don't do this in an orderly fashion; by the time you're done, you're not sure which program gave which information.

	For example, if your list is the following:

	pbga (66)
	xhth (57)
	ebii (61)
	havc (66)
	ktlj (57)
	fwft (72) -> ktlj, cntj, xhth
	qoyq (66)
	padx (45) -> pbga, havc, qoyq
	tknk (41) -> ugml, padx, fwft
	jptl (61)
	ugml (68) -> gyxo, ebii, jptl
	gyxo (61)
	cntj (57)

	...then you would be able to recreate the structure of the towers that looks like this:

					gyxo
				  /
			 ugml - ebii
		   /      \
		  |         jptl
		  |
		  |         pbga
		 /        /
	tknk --- padx - havc
		 \        \
		  |         qoyq
		  |
		  |         ktlj
		   \      /
			 fwft - cntj
				  \
					xhth

	In this example, tknk is at the bottom of the tower (the bottom program), and is holding up ugml, padx, and fwft.
	 Those programs are, in turn, holding up other programs; in this example, none of those programs are holding up
	 any other programs, and are all the tops of their own towers. (The actual tower balancing in front of you is
	 much larger.)

	Before you're ready to help them, you need to make sure your information is correct. What is the name of
	 the bottom program?

	 NOTE: Could just parse all program names and look for name that only occurs once.

	 * @param input Puzzle input
	 * @return Bottom program name
	 */
	std::string puzzle1(const std::vector<std::string> &input) {
		std::unordered_set<std::string> programs;
		std::unordered_set<std::string> children;
		for (std::size_t i = 0; i < input.size(); ++i) {
			const std::string &line = input[i];
			std::smatch matches;
			if (std::regex_search(line, matches, line_regex)) {
				if (matches.size() != 4) {
					throw std::runtime_error("Expected 4 matches, row: " + std::to_string(i));
				}
				if (!matches[1].matched) {
					throw std::invalid_argument("Unable to match group 1, row: " + std::to_string(i));
				}
				programs.insert(matches[1].str());
				if (matches.size() == 4 && matches[3].matched) {
					Util::split_string(matches[3], ", ", std::inserter(children, children.begin()));
				}
			} else {
				throw std::invalid_argument("Invalid input format, see example");
			}
		}
		std::unordered_set<std::string> program_root;
		for (const std::string &program : programs) {
			// if program is not listed as a child
			if (children.find(program) == children.cend()) {
				program_root.insert(program);
			}
		}
		if (program_root.empty()) {
			throw std::invalid_argument("No root found");
		}
		if (program_root.size() > 1) {
			throw std::invalid_argument("Multiple roots found");
		}
		return *program_root.cbegin();
	}

	/**
	 * The programs explain the situation: they can't get down. Rather, they could get down, if they weren't expending
	 * all of their energy trying to keep the tower balanced. Apparently, one program has the wrong weight, and until
	 * it's fixed, they're stuck here.

For any program holding a disc, each program standing on that disc forms a sub-tower. Each of those sub-towers are
	 supposed to be the same weight, or the disc itself isn't balanced. The weight of a tower is the sum of the weights
	 of the programs in that tower.

In the example above, this means that for ugml's disc to be balanced, gyxo, ebii, and jptl must all have the same
	 weight, and they do: 61.

However, for tknk to be balanced, each of the programs standing on its disc and all programs above it must each match.
	 This means that the following sums must all be the same:

    ugml + (gyxo + ebii + jptl) = 68 + (61 + 61 + 61) = 251
    padx + (pbga + havc + qoyq) = 45 + (66 + 66 + 66) = 243
    fwft + (ktlj + cntj + xhth) = 72 + (57 + 57 + 57) = 243

As you can see, tknk's disc is unbalanced: ugml's stack is heavier than the other two. Even though the nodes above
	 ugml are balanced, ugml itself is too heavy: it needs to be 8 units lighter for its stack to weigh 243 and keep
	 the towers balanced. If this change were made, its weight would be 60.

Given that exactly one program is the wrong weight, what would its weight need to be to balance the entire tower?

	 * @param input Puzzle input
	 * @return Unbalanced program balanced weight
	 */
	int puzzle2(const std::vector<std::string> &input) {
		Solver solver(input);
		return solver.puzzle2();
	}

	Solver::Solver(const std::vector<std::string> &input) {
		for (std::size_t i = 0; i < input.size(); ++i) {
			const std::string &line = input[i];
			std::smatch matches;
			if (std::regex_search(line, matches, line_regex)) {
				if (matches.size() != 4) {
					throw std::runtime_error("Expected 4 matches, row: " + std::to_string(i));
				}
				if (!matches[1].matched) {
					throw std::invalid_argument("Unable to match group 1, row: " + std::to_string(i));
				}
				const std::string program_name = matches[1];
				vertices[program_name] = std::stoi(matches[2]); // weight
				searched[program_name] = false;
				if (matches.size() == 4 && matches[3].matched) {
					std::vector<std::string> program_edges;
					Util::split_string(matches[3], ", ", std::inserter(program_edges, program_edges.begin()));
					edges.emplace(program_name, std::move(program_edges));
				}
			} else {
				throw std::invalid_argument("Invalid input format, see example");
			}
		}
	}

	int Solver::puzzle2() {
		for (const auto &vertex_pair : vertices) {
			calculate_cumulative_weight(vertex_pair.first);
			const int result = search(vertex_pair.first);
			if (result != 0) {
				return result;
			}
		}
		throw std::runtime_error("No solution");
	}

	int Solver::search(const std::string &root) {
		if (!searched[root]) {
			searched[root] = true;
			std::unordered_map<int, int> weight_frequency;
			for (const auto &edge : edges[root]) {
				++weight_frequency[cumulative_weights[edge]];
			}
			if (weight_frequency.size() > 1) {
				auto wf1 = weight_frequency.begin(), wf2 = std::next(weight_frequency.begin());
				if (wf2->second > wf1->second) {
					std::swap(wf1, wf2);
				}
				auto program = *std::find_if(edges[root].begin(), edges[root].end(), [this, wf2](const std::string &edge) {
					return cumulative_weights[edge] == wf2->first;
				});
				const int result = search(program);
				return result == 0
					   ? vertices[program] + wf1->first - wf2->first
					   : result;
			}
		}

		return 0;
	}

	int Solver::calculate_cumulative_weight(const std::string &root) {
		if (cumulative_weights[root] == 0) {
			cumulative_weights[root] = vertices[root]
									   + std::accumulate(edges[root].begin(), edges[root].end(), 0,
														 [this](const int weight, const auto &edge) {
															 return weight + calculate_cumulative_weight(edge);
														 });
		}
		return cumulative_weights[root];
	}
}
