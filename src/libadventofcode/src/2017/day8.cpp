#include "libadventofcode/2017/day8.hpp"

#include <sstream>
#include <unordered_map>
#include <algorithm>

namespace advent_of_code::y2017::day8 {
	bool register_conditional_check(const std::string &comparison_type, int current_value, int check_value);
	std::pair<int, int> solve_generic(const std::vector<std::vector<std::string>> &input);

	/**
	 * You receive a signal directly from the CPU. Because of your recent assistance with jump instructions,
	 * it would like you to compute the result of a series of unusual register instructions.

	Each instruction consists of several parts: the register to modify, whether to increase or decrease that
	 register's value, the amount by which to increase or decrease it, and a condition. If the condition fails,
	 skip the instruction without modifying the register. The registers all start at 0. The instructions look like this:

	b inc 5 if a > 1
	a inc 1 if b < 5
	c dec -10 if a >= 1
	c inc -20 if c == 10

	These instructions would be processed as follows:

		Because a starts at 0, it is not greater than 1, and so b is not modified.
		a is increased by 1 (to 1) because b is less than 5 (it is 0).
		c is decreased by -10 (to 10) because a is now greater than or equal to 1 (it is 1).
		c is increased by -20 (to -10) because c is equal to 10.

	After this process, the largest value in any register is 1.

	You might also encounter <= (less than or equal to) or != (not equal to). However, the CPU doesn't have the bandwidth
	 to tell you what all the registers are named, and leaves that to you to determine.

	What is the largest value in any register after completing the instructions in your puzzle input?

	 * @param input Instruction input
	 * @return Largest register value
	 */
	int puzzle1(const std::vector<std::vector<std::string>> &input) {
		return solve_generic(input).first;
	}

	/**
	 * To be safe, the CPU also needs to know the highest value held in any register during this process so that
	 * it can decide how much memory to allocate to these operations. For example, in the above instructions,
	 * the highest value ever held was 10 (in register c after the third instruction was evaluated).
	 *
	 * @param input Instruction input
	 * @return Largest historic (at any point in the process) register value
	 */
	int puzzle2(const std::vector<std::vector<std::string>> &input) {
		return solve_generic(input).second;
	}

	std::pair<int, int> solve_generic(const std::vector<std::vector<std::string>> &input) {
		std::unordered_map<std::string, int> register_map;
		int historic_max = 0;
		for (const auto &line : input) {
			if (line.size() != 7) {
				throw std::invalid_argument("Invalid input, each line must contain 7 tokens. See example.");
			}

			const int register_check_current_value = register_map[line[4]];
			const int register_check_test_value = std::stoi(line[6]);
			const std::string comparison_type = line[5];
			const bool conditional_check = register_conditional_check(comparison_type, register_check_current_value, register_check_test_value);
			if (!conditional_check) {
				continue;
			}

			int &register_value = register_map[line[0]];
			const int register_action_amount = std::stoi(line[2]);
			const std::string &register_action = line[1];
			if (register_action == "inc") {
				register_value += register_action_amount;
			} else if (register_action == "dec") {
				register_value -= register_action_amount;
			} else {
				throw std::invalid_argument("Invalid register action: " + register_action);
			}
			// update historic max
			// can decrement by a negative amount, so check after all register actions
			if (register_value > historic_max) {
				historic_max = register_value;
			}
		}

		const auto max = std::max_element(register_map.cbegin(),
										  register_map.cend(),
										  [](const auto &p1, const auto &p2) { return p1.second < p2.second; });
		return {max->second, historic_max};
	}

	bool register_conditional_check(const std::string &comparison_type, const int current_value,
									const int check_value) {
		if (comparison_type == ">") {
			return current_value > check_value;
		} else if (comparison_type == ">=") {
			return current_value >= check_value;
		} else if (comparison_type == "<") {
			return current_value < check_value;
		} else if (comparison_type == "<=") {
			return current_value <= check_value;
		} else if (comparison_type == "==") {
			return current_value == check_value;
		} else if (comparison_type == "!=") {
			return current_value != check_value;
		} else {
			throw std::invalid_argument("Invalid input, unrecognized comparison: " + comparison_type);
		}
	}
}
