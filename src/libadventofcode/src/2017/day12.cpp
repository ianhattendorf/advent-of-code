#include "libadventofcode/2017/day12.hpp"

#include <algorithm>
#include <regex>
#include <unordered_map>
#include <unordered_set>
#include <stack>

#include "libadventofcode/2017/util.hpp"

namespace advent_of_code::y2017::day12 {
	std::size_t solve_generic(const std::vector<std::string> &input, bool is_puzzle1);
	std::vector<std::vector<int>> read_input(const std::vector<std::string> &input);

	/**
	 * Walking along the memory banks of the stream, you find a small village that is experiencing a little confusion:
	 * some programs can't communicate with each other.

	Programs in this village communicate using a fixed system of pipes. Messages are passed between programs using
	 these pipes, but most programs aren't connected to each other directly. Instead, programs pass messages between
	 each other until the message reaches the intended recipient.

	For some reason, though, some of these messages aren't ever reaching their intended recipient, and the programs
	 suspect that some pipes are missing. They would like you to investigate.

	You walk through the village and record the ID of each program and the IDs with which it can communicate directly
	 (your puzzle input). Each program has one or more programs with which it can communicate, and these pipes are
	 bidirectional; if 8 says it can communicate with 11, then 11 will say it can communicate with 8.

	You need to figure out how many programs are in the group that contains program ID 0.

	For example, suppose you go door-to-door like a travelling salesman and record the following list:

	0 <-> 2
	1 <-> 1
	2 <-> 0, 3, 4
	3 <-> 2, 4
	4 <-> 2, 3, 6
	5 <-> 6
	6 <-> 4, 5

	In this example, the following programs are in the group that contains program ID 0:

		Program 0 by definition.
		Program 2, directly connected to program 0.
		Program 3 via program 2.
		Program 4 via program 2.
		Program 5 via programs 6, then 4, then 2.
		Program 6 via programs 4, then 2.

	Therefore, a total of 6 programs are in this group; all but program 1, which has a pipe that connects it to itself.

	How many programs are in the group that contains program ID 0?

	 * @param input Programing mappings
	 * @return Numbers of programs in group containing ID 0
	 */
	std::size_t puzzle1(const std::vector<std::string> &input) {
		return solve_generic(input, true);
	}

	/**
	 * There are more programs than just the ones in the group containing program ID 0. The rest of them have no way of
	 * reaching that group, and still might have no way of reaching each other.

	A group is a collection of programs that can all communicate via pipes either directly or indirectly. The programs you
	 identified just a moment ago are all part of the same group. Now, they would like you to determine the total number of groups.

	In the example above, there were 2 groups: one consisting of programs 0,2,3,4,5,6, and the other consisting solely of
	 program 1.

	How many groups are there in total?

	 * @param input Programing mappings
	 * @return Total number of groups
	 */
	std::size_t puzzle2(const std::vector<std::string> &input) {
		return solve_generic(input, false);
	}

	std::size_t solve_generic(const std::vector<std::string> &input, bool is_puzzle1) {
		const std::vector<std::vector<int>> programs = read_input(input);

		std::unordered_set<int> seen;
		std::stack<int> working;
		std::size_t number_of_groups = 0;

		for (std::size_t i = 0; i < programs.size(); ++i) {
			if (seen.find(static_cast<int>(i)) != seen.cend()) {
				continue;
			}
			working.push(static_cast<int>(i));
			++number_of_groups;
			while (!working.empty()) {
				const auto working_program = working.top();
				working.pop();
				if (!seen.insert(working_program).second) {
					continue;
				}
				for (const int program_linked : programs[static_cast<std::size_t>(working_program)]) {
					working.push(program_linked);
				}
			}
			if (is_puzzle1 && number_of_groups == 1) {
				return seen.size();
			}
		}

		return number_of_groups;
	}

	std::vector<std::vector<int>> read_input(const std::vector<std::string> &input) {
		const std::regex line_regex(R"delim(^(\d+)\s+<->\s+([\d\s,]+)$)delim");
		std::vector<std::vector<int>> programs;

		for (std::size_t i = 0; i < input.size(); ++i) {
			const std::string &line = input[i];
			std::smatch matches;
			if (std::regex_search(line, matches, line_regex)) {
				if (matches.size() != 3) {
					throw std::runtime_error("Expected 3 matches, row: " + std::to_string(i));
				}
				if (!matches[1].matched) {
					throw std::invalid_argument("Unable to match group 1, row: " + std::to_string(i));
				}
				if (!matches[2].matched) {
					throw std::invalid_argument("Unable to match group 2, row: " + std::to_string(i));
				}
				std::vector<int> connected_programs;
				advent_of_code::Util::split_string_int(matches[2].str(), ", ",
													   std::inserter(connected_programs, connected_programs.begin()));
				const int program_number = std::stoi(matches[1].str());
				if (program_number < 0) {
					throw std::invalid_argument("Found negative program number, row: " + std::to_string(i));
				}
				if (static_cast<int>(programs.size()) != program_number) {
					throw std::invalid_argument("Expected programs in sequential order (0, 1, 2, ..), row: "
												+ std::to_string(i));
				}
				programs.push_back(connected_programs);
			} else {
				throw std::invalid_argument("Invalid input format, see example");
			}
		}

		return programs;
	}
}
