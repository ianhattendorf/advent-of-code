#include "libadventofcode/2017/day6.hpp"

#include <stdexcept>
#include <unordered_set>
#include <unordered_map>
#include <limits>
#include <algorithm>
#include <iterator>

namespace advent_of_code::y2017::day6 {
	void allocate_banks(std::vector<int> &input);

	template <class t_vector_data>
	struct VectorHash {
		std::size_t operator()(const std::vector<t_vector_data> &v) const {
			std::hash<t_vector_data> hasher;
			std::size_t seed = v.size();
			for (t_vector_data i : v) { // reference for larger objects?
				seed ^= hasher(i) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
			}
			return seed;
		}
	};

	/**
	 * A debugger program here is having an issue: it is trying to repair a memory reallocation routine, but it keeps
	 * getting stuck in an infinite loop.

	In this area, there are sixteen memory banks; each memory bank can hold any number of blocks. The goal of the
	 reallocation routine is to balance the blocks between the memory banks.

	The reallocation routine operates in cycles. In each cycle, it finds the memory bank with the most blocks
	 (ties won by the lowest-numbered memory bank) and redistributes those blocks among the banks. To do this, it removes
	 all of the blocks from the selected bank, then moves to the next (by index) memory bank and inserts one of the blocks.
	 It continues doing this until it runs out of blocks; if it reaches the last memory bank, it wraps around to the first
	 one.

	The debugger would like to know how many redistributions can be done before a blocks-in-banks configuration is produced
	 that has been seen before.

	For example, imagine a scenario with only four memory banks:

		The banks start with 0, 2, 7, and 0 blocks. The third bank has the most blocks, so it is chosen for redistribution.
		Starting with the next bank (the fourth bank) and then continuing to the first bank, the second bank, and so on,
		the 7 blocks are spread out over the memory banks. The fourth, first, and second banks get two blocks each, and
		the third bank gets one back. The final result looks like this: 2 4 1 2.
		Next, the second bank is chosen because it contains the most blocks (four). Because there are four memory banks,
		each gets one block. The result is: 3 1 2 3.
		Now, there is a tie between the first and fourth memory banks, both of which have three blocks. The first bank wins
		the tie, and its three blocks are distributed evenly over the other three banks, leaving it with none: 0 2 3 4.
		The fourth bank is chosen, and its four blocks are distributed such that each of the four banks receives
		one: 1 3 4 1.
		The third bank is chosen, and the same thing happens: 2 4 1 2.

	At this point, we've reached a state we've seen before: 2 4 1 2 was already seen. The infinite loop is detected after
	 the fifth block redistribution cycle, and so the answer in this example is 5.

	Given the initial block counts in your puzzle input, how many redistribution cycles must be completed before a
	 configuration is produced that has been seen before?

	 * @param input Memory bank input
	 * @return Infinite loop iteration number
	 */
	unsigned int puzzle1(std::vector<int> input) {
		if (input.empty()) {
			throw std::invalid_argument("Input cannot be empty");
		}

		std::unordered_set<std::vector<int>, VectorHash<int>> bank_set;
		for (unsigned int iteration_count = 0; iteration_count < LOOP_UPPER_BOUND; ++iteration_count) {
			if (bank_set.find(input) != bank_set.cend()) {
				return iteration_count;
			}
			bank_set.insert(input);
			allocate_banks(input);
		}
		throw std::runtime_error("Reached loop upper bound: " + std::to_string(LOOP_UPPER_BOUND));
	}

	/**
	 * Out of curiosity, the debugger would also like to know the size of the loop: starting from a state that has already
	 * been seen, how many block redistribution cycles must be performed before that same state is seen again?

	In the example above, 2 4 1 2 is seen again after four cycles, and so the answer in that example would be 4.

	How many cycles are in the infinite loop that arises from the configuration in your puzzle input?

	 * @param input Memory bank input
	 * @return Infinite loop size
	 */
	unsigned int puzzle2(std::vector<int> input) {
		if (input.empty()) {
			throw std::invalid_argument("Input cannot be empty");
		}

		std::unordered_map<std::vector<int>, unsigned int, VectorHash<int>> bank_map;
		for (unsigned int iteration_count = 0; iteration_count < LOOP_UPPER_BOUND; ++iteration_count) {
			{
				auto found = bank_map.find(input);
				if (found != bank_map.cend()) {
					return iteration_count - found->second;
				}
			}
			bank_map.insert({input, iteration_count});
			allocate_banks(input);
		}
		throw std::runtime_error("Reached loop upper bound: " + std::to_string(LOOP_UPPER_BOUND));
	}

	void allocate_banks(std::vector<int> &input) {
		auto max = std::max_element(input.begin(), input.end());
		auto current = std::next(max);
		while (*max > 0) {
			if (current == input.end()) {
				current = input.begin();
			}
			++(*(current++));
			--(*max);
		}
	}
}
