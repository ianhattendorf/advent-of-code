#include "libadventofcode/2017/util.hpp"

#include <limits>
#include <string>
#include <stdexcept>

unsigned int advent_of_code::Util::stoui(const std::string &str, size_t *idx, const int base) {
	unsigned long result = std::stoul(str, idx, base);
	if (result > std::numeric_limits<unsigned int>::max()) {
		throw std::out_of_range("stoui: " + str + " out of range");
	}
	return static_cast<unsigned int>(result);
}
