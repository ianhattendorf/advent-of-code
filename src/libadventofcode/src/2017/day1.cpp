#include <string>
#include <iostream>
#include <exception>
#include <functional>

#include "libadventofcode/2017/day1.hpp"

namespace advent_of_code::y2017::day1 {
	void puzzle1_check_input(const std::string &input);
	unsigned int solve_generic(const std::string &input,
							   const std::function<unsigned int(std::size_t)> &next_digit_function);

	/**
	 * The captcha requires you to review a sequence of digits (your puzzle input) and find the sum of all digits that
	 * match the next digit in the list. The list is circular, so the digit after the last digit is the first digit in
	 * the list.

	For example:

		1122 produces a sum of 3 (1 + 2) because the first digit (1) matches the second digit and the third digit (2)
		matches the fourth digit.
		1111 produces 4 because each digit (all 1) matches the next.
		1234 produces 0 because no digit matches the next.
		91212129 produces 9 because the only digit that matches the next one is the last digit, 9.
	 * @param input Captcha input
	 * @return Captcha solution
	 */
	unsigned int puzzle1(const std::string &input){
		return solve_generic(input, [&input](std::size_t i) { return i + 1; });
	}

	/**
	 * Now, instead of considering the next digit, it wants you to consider the digit halfway around the circular list.
	 * That is, if your list contains 10 items, only include a digit in your sum if the digit 10/2 = 5 steps forward
	 * matches it. Fortunately, your list has an even number of elements.

	For example:

		1212 produces 6: the list contains 4 items, and all four digits match the digit 2 items ahead.
		1221 produces 0, because every comparison is between a 1 and a 2.
		123425 produces 4, because both 2s match each other, but no other digit has a match.
		123123 produces 12.
		12131415 produces 4.
	 * @param input Captcha input
	 * @return Captcha solution
	 */
	unsigned int puzzle2(const std::string &input) {
		if (input.size() % 2 == 1) {
			throw std::invalid_argument("Expected even number of characters");
		}

		return solve_generic(input, [&input](std::size_t i) { return i + input.length() / 2; });
	}

	void puzzle1_check_input(const std::string &input) {
		for (const char &c : input) {
			if (c < '0' || c > '9') {
				throw std::invalid_argument("Input must consist of the digits 0-9");
			}
		}
	}

	unsigned int solve_generic(const std::string &input,
							   const std::function<unsigned int(std::size_t)> &next_digit_function) {
		if (input.size() < 2) {
			return 0;
		}
		puzzle1_check_input(input);

		unsigned int sum = 0;
		for (std::size_t i = 0; i < input.size(); ++i) {
			const char current_digit = input[i];
			const char next_digit = input[next_digit_function(i) % input.size()];
			if (current_digit == next_digit) {
				sum += static_cast<unsigned int>(current_digit - '0');
			}
		}

		return sum;
	}
}
