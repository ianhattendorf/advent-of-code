#include "catch.hpp"

#include "libadventofcode/2017/day2.hpp"
#include "test_util.hpp"

TEST_CASE("advent_of_code::y2017::day2::puzzle1 is calculated") {
	auto day2_input = advent_of_code::y2017::TestUtil::read_matrix_file<int>(
			"../src/libadventofcode/test/2017/day2_input.txt");
	REQUIRE(advent_of_code::y2017::day2::puzzle1({}) == 0); // empty matrix
	REQUIRE(advent_of_code::y2017::day2::puzzle1({{5, 1, 9, 5}, {}, {2, 4, 6, 8}}) == 14); // empty line in matrix
	REQUIRE(advent_of_code::y2017::day2::puzzle1({{5, 1, 9, 5}, {7, 5, 3}, {2, 4, 6, 8}}) == 18); // given example
	REQUIRE(advent_of_code::y2017::day2::puzzle1(day2_input) == 36766); // challenge input
}

TEST_CASE("advent_of_code::y2017::day2::puzzle2 is calculated") {
	auto day2_input = advent_of_code::y2017::TestUtil::read_matrix_file<int>(
			"../src/libadventofcode/test/2017/day2_input.txt");
	REQUIRE(advent_of_code::y2017::day2::puzzle2({}) == 0); // empty matrix
	REQUIRE(advent_of_code::y2017::day2::puzzle2({{5, 9, 2, 8}, {}, {3, 8, 6, 5}}) == 6); // empty line in matrix
	REQUIRE(advent_of_code::y2017::day2::puzzle2({{5, 9, 2, 8}, {9, 4, 7, 3}, {3, 8, 6, 5}}) == 9); // given example
	REQUIRE(advent_of_code::y2017::day2::puzzle2(day2_input) == 261); // challenge input
}
