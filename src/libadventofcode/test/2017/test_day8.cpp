#include <string>

#include "catch.hpp"

#include "libadventofcode/2017/day8.hpp"
#include "test_util.hpp"

using advent_of_code::y2017::TestUtil;

TEST_CASE("advent_of_code::y2017::day8::puzzle1 is calculated") {
	auto input_example = TestUtil::read_matrix_file<std::string>("../src/libadventofcode/test/2017/day8_input_example.txt");
	auto input = TestUtil::read_matrix_file<std::string>("../src/libadventofcode/test/2017/day8_input.txt");
	REQUIRE(advent_of_code::y2017::day8::puzzle1(input_example) == 1);
	REQUIRE(advent_of_code::y2017::day8::puzzle1(input) == 6611);
}

TEST_CASE("advent_of_code::y2017::day8::puzzle1 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day8::puzzle1({{"b", "inc", "5", "if", "a", "<"}}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day8::puzzle1({{"b", "inc", "5", "if", "a", "!", "1"}}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day8::puzzle1({{"b", "idk", "5", "if", "a", "<", "1"}}), std::invalid_argument);
}

TEST_CASE("advent_of_code::y2017::day8::puzzle2 is calculated") {
	auto input_example = TestUtil::read_matrix_file<std::string>("../src/libadventofcode/test/2017/day8_input_example.txt");
	auto input = TestUtil::read_matrix_file<std::string>("../src/libadventofcode/test/2017/day8_input.txt");
	REQUIRE(advent_of_code::y2017::day8::puzzle2(input_example) == 10);
	REQUIRE(advent_of_code::y2017::day8::puzzle2(input) == 6619);
}

TEST_CASE("advent_of_code::y2017::day8::puzzle2 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day8::puzzle2({{"b", "inc", "5", "if", "a", "<"}}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day8::puzzle2({{"b", "inc", "5", "if", "a", "!", "1"}}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day8::puzzle2({{"b", "idk", "5", "if", "a", "<", "1"}}), std::invalid_argument);
}
