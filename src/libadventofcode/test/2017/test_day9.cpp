#include "catch.hpp"

#include "libadventofcode/2017/day9.hpp"
#include "test_util.hpp"

using advent_of_code::y2017::TestUtil;

TEST_CASE("advent_of_code::y2017::day9::puzzle1 is calculated") {
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day9_input.txt");
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{}") == 1);
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{{{}}}") == 6);
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{{},{}}") == 5);
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{{{},{},{{}}}}") == 16);
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{<a>,<a>,<a>,<a>}") == 1);
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{{<ab>},{<ab>},{<ab>},{<ab>}}") == 9);
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{{<!!>},{<!!>},{<!!>},{<!!>}}") == 9);
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{{<!!>},{<!!>},{<!!>},{<!!>}}") == 9);
	REQUIRE(advent_of_code::y2017::day9::puzzle1("{{<a!>},{<a!>},{<a!>},{<ab>}}") == 3);
	REQUIRE(advent_of_code::y2017::day9::puzzle1(input.front()) == 15922);
}

TEST_CASE("advent_of_code::y2017::day9::puzzle1 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day9::puzzle1("{}}"), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day9::puzzle1("{{}"), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day9::puzzle1("<"), std::invalid_argument);
}

TEST_CASE("advent_of_code::y2017::day9::puzzle2 is calculated") {
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day9_input.txt");
	REQUIRE(advent_of_code::y2017::day9::puzzle2("<>") == 0);
	REQUIRE(advent_of_code::y2017::day9::puzzle2("<random characters>") == 17);
	REQUIRE(advent_of_code::y2017::day9::puzzle2("<<<<>") == 3);
	REQUIRE(advent_of_code::y2017::day9::puzzle2("<{!>}>") == 2);
	REQUIRE(advent_of_code::y2017::day9::puzzle2("<!!>") == 0);
	REQUIRE(advent_of_code::y2017::day9::puzzle2("<!!!>>") == 0);
	REQUIRE(advent_of_code::y2017::day9::puzzle2("<{o\"i!a,<{i<a>") == 10);
	REQUIRE(advent_of_code::y2017::day9::puzzle2(input.front()) == 7314);
}

TEST_CASE("advent_of_code::y2017::day9::puzzle2 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day9::puzzle2("{}}"), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day9::puzzle2("{{}"), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day9::puzzle2("<"), std::invalid_argument);
}
