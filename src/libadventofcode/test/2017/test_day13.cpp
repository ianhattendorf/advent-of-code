#include "catch.hpp"

#include "libadventofcode/2017/day13.hpp"
#include "test_util.hpp"

using advent_of_code::y2017::TestUtil;

TEST_CASE("advent_of_code::y2017::day13::puzzle1 is calculated") {
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day13_input.txt");
	REQUIRE(advent_of_code::y2017::day13::puzzle1({"0: 3", "1: 2", "4: 4", "6: 4"}) == 24);
	REQUIRE(advent_of_code::y2017::day13::puzzle1(input) == 1588);
}

TEST_CASE("advent_of_code::y2017::day13::puzzle1 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day13::puzzle1({"asdf"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day13::puzzle1({"a: 1"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day13::puzzle1({"1: a"}), std::invalid_argument);
}

TEST_CASE("advent_of_code::y2017::day13::puzzle2 is calculated") {
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day13_input.txt");
	REQUIRE(advent_of_code::y2017::day13::puzzle2({"0: 3", "1: 2", "4: 4", "6: 4"}) == 10);
	REQUIRE(advent_of_code::y2017::day13::puzzle2(input) == 3865118);
}

TEST_CASE("advent_of_code::y2017::day13::puzzle2 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day13::puzzle2({"asdf"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day13::puzzle2({"a: 1"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day13::puzzle2({"1: a"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day13::puzzle2({"0: 2", "1: 2"}), std::invalid_argument); // no end
}
