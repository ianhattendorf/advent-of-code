#include "catch.hpp"

#include "libadventofcode/2017/day6.hpp"
#include "test_util.hpp"

TEST_CASE("advent_of_code::y2017::day6::puzzle1 is calculated") {
	auto input = advent_of_code::y2017::TestUtil::read_array_file<int>(
			"../src/libadventofcode/test/2017/day6_input.txt");
	REQUIRE(advent_of_code::y2017::day6::puzzle1({0, 2, 7, 0}) == 5);
	REQUIRE(advent_of_code::y2017::day6::puzzle1(input) == 5042);
}

TEST_CASE("advent_of_code::y2017::day6::puzzle1 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day6::puzzle1({}), std::invalid_argument);
}

TEST_CASE("advent_of_code::y2017::day6::puzzle2 is calculated") {
	auto input = advent_of_code::y2017::TestUtil::read_array_file<int>(
			"../src/libadventofcode/test/2017/day6_input.txt");
	REQUIRE(advent_of_code::y2017::day6::puzzle2({0, 2, 7, 0}) == 4);
	REQUIRE(advent_of_code::y2017::day6::puzzle2(input) == 1086);
}

TEST_CASE("advent_of_code::y2017::day6::puzzle2 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day6::puzzle2({}), std::invalid_argument);
}
