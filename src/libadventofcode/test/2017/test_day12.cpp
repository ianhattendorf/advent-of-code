#include "catch.hpp"

#include "libadventofcode/2017/day12.hpp"
#include "test_util.hpp"

using advent_of_code::y2017::TestUtil;

TEST_CASE("advent_of_code::y2017::day12::puzzle1 is calculated") {
	auto input_example = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day12_input_example.txt");
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day12_input.txt");
	REQUIRE(advent_of_code::y2017::day12::puzzle1(input_example) == 6);
	REQUIRE(advent_of_code::y2017::day12::puzzle1(input) == 152);
}

TEST_CASE("advent_of_code::y2017::day12::puzzle1 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle1({"asdf"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle1({"a -> 1, 2"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle1({"0 -> 1, a"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle1({"0 -> 1, a"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle1({"0 -> 1, -1"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle1({"0 -> 1, 2", "-1 -> 1, 3"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle1({"3 -> 1, 2", "4 -> 1, 3"}), std::invalid_argument);
}

TEST_CASE("advent_of_code::y2017::day12::puzzle2 is calculated") {
	auto input_example = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day12_input_example.txt");
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day12_input.txt");
	REQUIRE(advent_of_code::y2017::day12::puzzle2(input_example) == 2);
	REQUIRE(advent_of_code::y2017::day12::puzzle2(input) == 186);
}

TEST_CASE("advent_of_code::y2017::day12::puzzle2 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle2({"asdf"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle2({"a -> 1, 2"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle2({"0 -> 1, a"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle2({"0 -> 1, a"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle2({"0 -> 1, -1"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle2({"0 -> 1, 2", "-1 -> 1, 3"}), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day12::puzzle2({"3 -> 1, 2", "4 -> 1, 3"}), std::invalid_argument);
}
