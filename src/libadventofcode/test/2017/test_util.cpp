#include "test_util.hpp"

std::vector<std::string> advent_of_code::y2017::TestUtil::read_file_by_line(const std::string &path) {
	std::ifstream file(path);

	if (!file) {
		throw std::invalid_argument("Couldn't find file: " + path);
	}

	std::vector<std::string> lines;
	for (std::string line; std::getline(file, line); /**/) {
		lines.push_back(std::move(line));
	}

	return lines;
}
