#include <string>

#include "catch.hpp"

#include "libadventofcode/2017/day7.hpp"
#include "test_util.hpp"

using advent_of_code::y2017::TestUtil;

TEST_CASE("advent_of_code::y2017::day7::puzzle1 is calculated") {
	auto input_example = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day7_input_example.txt");
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day7_input.txt");
	REQUIRE(advent_of_code::y2017::day7::puzzle1(input_example) == "tknk");
	REQUIRE(advent_of_code::y2017::day7::puzzle1(input) == "bpvhwhh");
}

TEST_CASE("advent_of_code::y2017::day7::puzzle2 is calculated") {
	auto input_example = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day7_input_example.txt");
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day7_input.txt");
	REQUIRE(advent_of_code::y2017::day7::puzzle2(input_example) == 60);
	REQUIRE(advent_of_code::y2017::day7::puzzle2(input) == 256);
}
