#include "catch.hpp"

#include "libadventofcode/2017/day5.hpp"
#include "test_util.hpp"

TEST_CASE("advent_of_code::y2017::day5::puzzle1 is calculated") {
	auto input = advent_of_code::y2017::TestUtil::read_array_file<int>(
			"../src/libadventofcode/test/2017/day5_input.txt");
	REQUIRE(advent_of_code::y2017::day5::puzzle1({}) == 0); // empty matrix
	REQUIRE(advent_of_code::y2017::day5::puzzle1({0, 3, 0, 1, -3}) == 5);
	REQUIRE(advent_of_code::y2017::day5::puzzle1(input) == 373160);
}

TEST_CASE("advent_of_code::y2017::day5::puzzle2 is calculated") {
	auto input = advent_of_code::y2017::TestUtil::read_array_file<int>(
			"../src/libadventofcode/test/2017/day5_input.txt");
	REQUIRE(advent_of_code::y2017::day5::puzzle2({}) == 0); // empty matrix
	REQUIRE(advent_of_code::y2017::day5::puzzle2({0, 3, 0, 1, -3}) == 10);
	REQUIRE(advent_of_code::y2017::day5::puzzle2(input) == 26395586);
}
