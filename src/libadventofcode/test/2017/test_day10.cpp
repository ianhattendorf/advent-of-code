#include "catch.hpp"

#include "libadventofcode/2017/day10.hpp"
#include "test_util.hpp"

using advent_of_code::y2017::TestUtil;

TEST_CASE("advent_of_code::y2017::day10::puzzle1 is calculated") {
	auto input = TestUtil::read_array_file<std::size_t>("../src/libadventofcode/test/2017/day10_input.txt");
	REQUIRE(advent_of_code::y2017::day10::puzzle1({3, 4, 1, 5}, 5) == 12);
	REQUIRE(advent_of_code::y2017::day10::puzzle1(input, 256) == 38628);
}

TEST_CASE("advent_of_code::y2017::day10::puzzle1 handles invalid input") {
	REQUIRE_THROWS_AS(advent_of_code::y2017::day10::puzzle1({3, 4, 1, 5}, 1), std::invalid_argument);
	REQUIRE_THROWS_AS(advent_of_code::y2017::day10::puzzle1({3, 4, 1, 5}, 0), std::invalid_argument);
}

TEST_CASE("advent_of_code::y2017::day10::puzzle2 is calculated") {
	auto input = TestUtil::read_file_by_line("../src/libadventofcode/test/2017/day10_input_raw.txt");
	REQUIRE(advent_of_code::y2017::day10::puzzle2("") == "a2582a3a0e66e6e86e3812dcb672a272");
	REQUIRE(advent_of_code::y2017::day10::puzzle2("AoC 2017") == "33efeb34ea91902bb2f59c9920caa6cd");
	REQUIRE(advent_of_code::y2017::day10::puzzle2(" AoC 2017  ") == "33efeb34ea91902bb2f59c9920caa6cd");
	REQUIRE(advent_of_code::y2017::day10::puzzle2("1,2,3") == "3efbe78a8d82f29979031a4aa0b16a9d");
	REQUIRE(advent_of_code::y2017::day10::puzzle2("1,2,4") == "63960835bcdc130f0b66d7ff4f6a5a8e");
	REQUIRE(advent_of_code::y2017::day10::puzzle2(input.front()) == "e1462100a34221a7f0906da15c1c979a");
}
