#include "catch.hpp"

#include "libadventofcode/2017/day4.hpp"
#include "test_util.hpp"

TEST_CASE("advent_of_code::y2017::day4::puzzle1 is calculated") {
	auto day4_input = advent_of_code::y2017::TestUtil::read_matrix_file<std::string>(
			"../src/libadventofcode/test/2017/day4_input.txt");
	REQUIRE(advent_of_code::y2017::day4::puzzle1({}) == 0); // empty matrix
	REQUIRE(advent_of_code::y2017::day4::puzzle1({{"aa", "bb", "cc", "dd", "ee"}, {}}) == 1); // empty line in matrix
	REQUIRE(advent_of_code::y2017::day4::puzzle1({{"aa", "bb", "cc", "dd", "ee"}}) == 1);
	REQUIRE(advent_of_code::y2017::day4::puzzle1({{"aa", "bb", "cc", "dd", "aa"}}) == 0);
	REQUIRE(advent_of_code::y2017::day4::puzzle1({{"aa", "bb", "cc", "dd", "aaa"}}) == 1);
	REQUIRE(advent_of_code::y2017::day4::puzzle1(day4_input) == 383); // challenge input
}

TEST_CASE("advent_of_code::y2017::day4::puzzle2 is calculated") {
	auto day4_input = advent_of_code::y2017::TestUtil::read_matrix_file<std::string>(
			"../src/libadventofcode/test/2017/day4_input.txt");
	REQUIRE(advent_of_code::y2017::day4::puzzle2({}) == 0); // empty matrix
	REQUIRE(advent_of_code::y2017::day4::puzzle2({{"abcde", "fghij"}, {}}) == 1); // empty line in matrix
	REQUIRE(advent_of_code::y2017::day4::puzzle2({{"abcde", "fghij"}}) == 1);
	REQUIRE(advent_of_code::y2017::day4::puzzle2({{"abcde", "xyz", "ecdab"}}) == 0);
	REQUIRE(advent_of_code::y2017::day4::puzzle2({{"a", "ab", "abc", "abd", "abf", "abj"}}) == 1);
	REQUIRE(advent_of_code::y2017::day4::puzzle2({{"iiii", "oiii", "ooii", "oooi", "oooo"}}) == 1);
	REQUIRE(advent_of_code::y2017::day4::puzzle2({{"oiii", "ioii", "iioi", "iiio"}}) == 0);
	REQUIRE(advent_of_code::y2017::day4::puzzle2(day4_input) == 265); // challenge input
}
