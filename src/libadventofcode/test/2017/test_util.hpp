#ifndef ADVENT_OF_CODE_TEST_UTIL_HPP
#define ADVENT_OF_CODE_TEST_UTIL_HPP

#include <vector>
#include <string>

namespace advent_of_code {
	namespace y2017 {
		class TestUtil {
		public:
			template <class t_matrix_data>
			static std::vector<std::vector<t_matrix_data>> read_matrix_file(const std::string &path);
			template <class t_matrix_data>
			static std::vector<t_matrix_data> read_array_file(const std::string &path);
			static std::vector<std::string> read_file_by_line(const std::string &path);
		};
	}
}

#include <sstream>
#include <fstream>
#include <iterator>
#include <algorithm>

template <class t_matrix_data>
std::vector<std::vector<t_matrix_data>> advent_of_code::y2017::TestUtil::read_matrix_file(const std::string &path) {
	std::ifstream file(path);

	if (!file) {
		throw std::invalid_argument("Couldn't find file: " + path);
	}

	std::vector<std::vector<t_matrix_data>> lines;
	for (std::string line; std::getline(file, line); /**/) {
		std::istringstream line_stream(line);
		std::vector<t_matrix_data> line_vector;
		std::copy(std::istream_iterator<t_matrix_data>(line_stream),
				  std::istream_iterator<t_matrix_data>(),
				  std::back_inserter(line_vector));
		lines.push_back(line_vector);
	}

	return lines;
}

template <class t_matrix_data>
std::vector<t_matrix_data> advent_of_code::y2017::TestUtil::read_array_file(const std::string &path) {
	std::ifstream file(path);

	if (!file) {
		throw std::invalid_argument("Couldn't find file: " + path);
	}

	std::vector<t_matrix_data> data((std::istream_iterator<t_matrix_data>(file)), // vexing parse...
									std::istream_iterator<t_matrix_data>());

	return data;
}

#endif //ADVENT_OF_CODE_TEST_UTIL_HPP
