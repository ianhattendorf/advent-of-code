#ifndef ADVENT_OF_CODE_DAY2_HPP
#define ADVENT_OF_CODE_DAY2_HPP

#include <string>
#include <vector>

namespace advent_of_code::y2017::day2 {
	int puzzle1(const std::vector<std::vector<int>> &input);
	int puzzle2(const std::vector<std::vector<int>> &input);
}

#endif //ADVENT_OF_CODE_DAY2_HPP
