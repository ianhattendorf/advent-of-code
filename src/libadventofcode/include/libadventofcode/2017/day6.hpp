#ifndef ADVENT_OF_CODE_DAY6_HPP
#define ADVENT_OF_CODE_DAY6_HPP

#include <vector>

namespace advent_of_code::y2017::day6 {
	const unsigned int LOOP_UPPER_BOUND = 100'000;
	unsigned int puzzle1(std::vector<int> input);
	unsigned int puzzle2(std::vector<int> input);
}

#endif //ADVENT_OF_CODE_DAY6_HPP
