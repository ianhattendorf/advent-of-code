#ifndef ADVENT_OF_CODE_DAY3_HPP
#define ADVENT_OF_CODE_DAY3_HPP

#include <string>
#include <vector>

namespace advent_of_code::y2017::day4 {
    unsigned int puzzle1(const std::vector<std::vector<std::string>> &input);
    unsigned int puzzle2(const std::vector<std::vector<std::string>> &input);
}

#endif //ADVENT_OF_CODE_DAY3_HPP
