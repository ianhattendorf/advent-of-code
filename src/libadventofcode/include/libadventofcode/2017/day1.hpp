#ifndef ADVENT_OF_CODE_DAY1_HPP
#define ADVENT_OF_CODE_DAY1_HPP

#include <string>

namespace advent_of_code::y2017::day1 {
	unsigned int puzzle1(const std::string &input);
	unsigned int puzzle2(const std::string &input);
}

#endif //ADVENT_OF_CODE_DAY1_HPP
