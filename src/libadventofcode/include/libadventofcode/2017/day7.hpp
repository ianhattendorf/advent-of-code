#ifndef ADVENT_OF_CODE_DAY7_HPP
#define ADVENT_OF_CODE_DAY7_HPP

#include <string>
#include <vector>
#include <unordered_map>

namespace advent_of_code::y2017::day7 {
	std::string puzzle1(const std::vector<std::string> &input);
	int puzzle2(const std::vector<std::string> &input);

	class Solver {
	public:
		explicit Solver(const std::vector<std::string> &input);
		int puzzle2();
	private:
		int search(const std::string &root);
		int calculate_cumulative_weight(const std::string &root);
		std::unordered_map<std::string, int> vertices;
		std::unordered_map<std::string, std::vector<std::string>> edges;
		std::unordered_map<std::string, bool> searched;
		std::unordered_map<std::string, int> cumulative_weights;
	};
}

#endif //ADVENT_OF_CODE_DAY7_HPP
