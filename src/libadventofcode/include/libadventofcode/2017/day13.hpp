#ifndef ADVENT_OF_CODE_DAY13_HPP
#define ADVENT_OF_CODE_DAY13_HPP

#include <vector>
#include <string>

namespace advent_of_code::y2017::day13 {
	constexpr unsigned int MAX_PICO = 10'000'000;
	unsigned int puzzle1(const std::vector<std::string> &input);
	unsigned int puzzle2(const std::vector<std::string> &input);
}

#endif //ADVENT_OF_CODE_DAY13_HPP
