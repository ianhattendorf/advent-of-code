#ifndef ADVENT_OF_CODE_DAY8_HPP
#define ADVENT_OF_CODE_DAY8_HPP

#include <string>
#include <vector>

namespace advent_of_code::y2017::day8 {
	int puzzle1(const std::vector<std::vector<std::string>> &input);
	int puzzle2(const std::vector<std::vector<std::string>> &input);
}

#endif //ADVENT_OF_CODE_DAY8_HPP
