#ifndef ADVENT_OF_CODE_UTIL_HPP
#define ADVENT_OF_CODE_UTIL_HPP

#include <string>
#include <functional>
#include <algorithm>
#include <iterator>
#include <iostream>

namespace advent_of_code {
	class Util {
	public:
		template <class t_out>
		static void split_string(const std::string &input, const std::string &delimiters, t_out result);
		template <class t_out>
		static void split_string_int(const std::string &input, const std::string &delimiters, t_out result);
		template <typename t_input_iterator, typename t_data = typename std::iterator_traits<t_input_iterator>::value_type>
		static void print_container(t_input_iterator begin, t_input_iterator end, std::ostream &out_stream = std::cout, const std::string &delim = " ");
		static unsigned int stoui(const std::string &str, std::size_t *idx = 0, int base = 10);
	};
}

/**
 * Split string by delimiter(s)
 * @tparam t_out Output inserter
 * @param input String to split
 * @param delimiters Delimiter characters to split by
 * @param result Insert iterator to output results to
 */
template <class t_out>
void advent_of_code::Util::split_string(const std::string &input, const std::string &delimiters, t_out result) {
	std::size_t previous = 0, position;
	while ((position = input.find_first_of(delimiters, previous)) != std::string::npos) {
		if (position > previous) {
			*(result++) = input.substr(previous, position - previous);
		}
		previous = position + 1;
	}
	if (previous < input.length()) {
		*(result++) = input.substr(previous, std::string::npos);
	}
}

/**
 * Split string by delimiter(s)
 * @tparam t_out Output inserter
 * @param input String to split
 * @param delimiters Delimiter characters to split by
 * @param result Insert iterator to output results to
 */
template <class t_out>
void advent_of_code::Util::split_string_int(const std::string &input, const std::string &delimiters, t_out result) {
	std::size_t previous = 0, position;
	while ((position = input.find_first_of(delimiters, previous)) != std::string::npos) {
		if (position > previous) {
			*(result++) = std::stoi(input.substr(previous, position - previous));
		}
		previous = position + 1;
	}
	if (previous < input.length()) {
		*(result++) = std::stoi(input.substr(previous, std::string::npos));
	}
}

template<typename t_input_iterator, typename t_data>
void advent_of_code::Util::print_container(t_input_iterator begin, t_input_iterator end, std::ostream &out_stream,
										   const std::string &delim) {
	std::copy(begin, end, std::ostream_iterator<t_data>(out_stream, delim.c_str()));
}

#endif //ADVENT_OF_CODE_UTIL_HPP
