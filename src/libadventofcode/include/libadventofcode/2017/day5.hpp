#ifndef ADVENT_OF_CODE_DAY5_HPP
#define ADVENT_OF_CODE_DAY5_HPP

#include <vector>

namespace advent_of_code::y2017::day5 {
	unsigned int puzzle1(std::vector<int> input);
	unsigned int puzzle2(std::vector<int> input);
}

#endif //ADVENT_OF_CODE_DAY5_HPP
