#ifndef ADVENT_OF_CODE_DAY12_HPP
#define ADVENT_OF_CODE_DAY12_HPP

#include <vector>
#include <string>

namespace advent_of_code::y2017::day12 {
	std::size_t puzzle1(const std::vector<std::string> &input);
	std::size_t puzzle2(const std::vector<std::string> &input);
}

#endif //ADVENT_OF_CODE_DAY12_HPP
