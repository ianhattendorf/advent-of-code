#ifndef ADVENT_OF_CODE_DAY10_HPP
#define ADVENT_OF_CODE_DAY10_HPP

#include <vector>
#include <string>

namespace advent_of_code::y2017::day10 {
	std::size_t puzzle1(const std::vector<std::size_t> &lengths, std::size_t list_size);
	std::string puzzle2(std::string input);
}

#endif //ADVENT_OF_CODE_DAY10_HPP
