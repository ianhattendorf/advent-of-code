#ifndef ADVENT_OF_CODE_DAY9_HPP
#define ADVENT_OF_CODE_DAY9_HPP

#include <string>

namespace advent_of_code::y2017::day9 {
	unsigned int puzzle1(const std::string &input);
	unsigned int puzzle2(const std::string &input);
}

#endif //ADVENT_OF_CODE_DAY9_HPP
